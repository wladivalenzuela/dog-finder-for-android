# DISEÑO DE APLICACIÓN #

Este documento se presenta la idea general del proyecto, cuyos aspectos se considerarán como básicos para la aplicación final.

Como se expone en la documentación de Información, los objetivos principales son:

* Crear una aplicación para Android que ayude a encontrar perros perdidos de una forma fácil
* Ser gratis y libre de anuncios (muy importante)
* Funcionar en la mayoría de dispositivos Android

En base a éstos se planifican los aspectos que se detallan en esto documento.

## Motivación ##

#### Búsqueda de perros ####

Dentro de las distintas redes sociales existentes hasta hoy en día, destaca el uso de [Facebook](https://www.facebook.com/) para la búsqueda de animales perdidos. Lo anterior se efectúa mediante estados en los perfiles personales de las personas, publicaciones en páginas (fanpages) públicas o grupos (abiertos o cerrados) dedicados al propósito. 

El primer tipo de publicaciones puede resultar en poca difusión a exepción de casos puntuales de personas con más amigos quienes puedan difundor, dicho de otra forma proporcional a la cantidad de amigos o popularidad.

El segundo tipo de publicaciones la difusión es proporcional al número de seguidores de la página. Esto puede causar indirectamente un problema: Spam. El spam se da cuando una página dedicada (o inlcuso no) a la búsqueda de mascotas genera post de forma periódica, haciendo que los seguidores pasen por alto muchas de las publicaciones. Adicionalmente pierde pragmatismo al difundir información a personas que dificilmente podrán buscar una mascota por la evidente distancia geográfica.

Finalmente, la publicación de mascotas perdidas en grupos de facebooks específicos comparte muchos de los problemas presentados en el segundo tipo de publicaciones, pero a diferencia de estos los grupos tienen mucha menos visibilidad.

#### ¿ Por qué Android ? ####

Android ha demostrado ser una plataforma potente y accesible. Podemos arriesgarnos a decir que todo quien está preocupado de la problemática de perros ( o mascotas ) extraviadas posee un dispositivo con Android (celular mayormente). Más aún, podemos asegurar que gracias a los alcances tecnológicos la gran mayoría cuenta con acceso a Internet móvil.

Lo anterior, sumado a la gran flexibilidad, disponibilidad de documentación, acceso y fuentes de información dedicadas al desarrollo de aplicaciones para Android ([Android Studio](https://developer.android.com/studio/index.html?hl=es-419)), lo hacen un candidato perfecto para lograr los objetivos planteados.

### Estado del arte ###

En PlayStore podemos encontrar dos aplicaciones:

* [Wof](https://play.google.com/store/apps/details?id=com.ibex.wof&hl=es) ([Facebook](https://www.facebook.com/Siguiendo-Huellitas-921087054597847/))
* [Siguiendo Huellitas](https://play.google.com/store/apps/details?id=com.threestarssoftnew.siguiendohuellitas) ([Facebook](https://www.facebook.com/somoswof/))

##### WOF #####

Fortalezas:

* Gestión de datos de usuario (nombre, correo, contraseña, recuperar contraseña, etc)
* Configuración adecuada (seleccionar qué información mostrar a otros usuarios, activar/desactivar notificaciones al celular y email, permitir seguidores, permitir comentarios en reporte, seguir reportes)
* Pestaña de notificaciones
* Interfaz con scrolling intuitiva

Debilidades:

* Exigencia de GPS
* No existen etiquetas de búsqueda
* Falta un buscador con detalles por mascota
* Muestra información de reportes a distancias físicamente imposibles de seguir (más de 200kms)
* No es claro en los reportes

##### Siguiendo huellitas #####

Fortalezas:

* Distintas opciones de selección de acción (reportar perdido, ver perdidos, ver encontrados, ver en adopción, ver recuperados)
* Interfaz intuitiva
* Liviano
* Permite el uso integrado de alguna red social (Facebook, Google, Twitter)

Debilidades:

* Interfaz poco amigable al ojo
* No permite crear nuevas cuentas desde la aplicación
* Exige el uso de alguna red social (Facebook, Google, Twitter)


## Funcionalidades ##

#### Usuarios #####

La aplicación debe permitir el uso de usuarios autentificados. Es una exigencia para evitar que la aplicación se utilice en malas prácticas. Lo mínimo es permitir el registro de usuario nuevo por parte de la aplicación (nombre de usuario, contraseña y correo). Alternativamente puede existir la opción de autentificación mediante redes sociales (Facebook, Instagram, Twitter, Google).

Una opción para evitar fraudes, es la autentificación por medio de número telefónico (como lo hace WhatsApp).

#### Servicios disponibles ####

Los servicios mínimos que se deben entregar son: reportar perdido, reporte de avistamientos, buscar perdidos y informe de reporte encontrado. 

* El reporte de perros (o mascotas) debe permitir al usuario subir una imagen con la descripción correspondiente de que se ha extraviado. 
* El reporte de avistamiento permite al usuario reportar un posible caso de extravío, esto es, cuando se ve un perro (o mascota) aparentemente perdido, esto implica no ser el dueño del mismo. 
* La búsqueda de perros (o mascotas) debe permitir al usuario ver las imágenes y descripciones de los reportes de perdidos y los reportes de avistamientos. 
* El informe de reporte encontrado debe permitir al usuario indicar que ha rescatado a un perro (o mascota) en evidente estado de pérdida, ya sea por información obtenida desde la misma aplicación o collar.

#### Base de datos ####

La aplicación debe tener acceso a una base de datos. Se considera como principal apoyo [Firebase](https://firebase.google.com/?hl=es-419).

Firebase dentro de sus [herramientas](https://firebase.google.com/products/?hl=es-419) se destacan:

* Autentificación de usuario
* Base de datos en tiempo real
* Autentificación
* Funciones de nube (cloud storage)
* Cloud messaging (mensajes y notificaciones a los usuarios)


***
*Este documento será actualizado periodicamente*