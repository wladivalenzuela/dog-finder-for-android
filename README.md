# INFORMACIÓN #

Este documento describe la idea principal del proyecto.

### ¿ Para qué es este repositorio ? ###

* Crear una aplicación para Android que ayude a encontrar perros perdidos de una forma fácil
* Ser gratis y libre de anuncios (muy importante)
* Funcionar en la mayoría de dispositivos Android

### ¿ Con quién contactarse ? ###

* Wladimir Valenzuela (owner)

### Otros ###

* Más información sobre el proyecto [aquí](..overview/generaldesign.md)
* Este documento será actualizado periódicamente
# README #

This readme describes the main ideas for this project.

### What is this repository for? ###

* Create an Android application for finding dogs in a easy way
* Be free of charge and ads-free (most important)
* Be able to run in most of the Adnroid devices

### Who do I talk to? ###

* Wladimir Valenzuela (owner)

### More info ###

* This document will be updated periodically